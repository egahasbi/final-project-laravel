# Final Project

## Kelompok 10

## Anggota Kelompok
* Ega Hasbi Rizqullah
* Danang Iman Panuluh

## Tema Project
Forum discussion about outer space, astronomy, science, and any related things.

## ERD
<img src="ERDFinalProject.png">

## Link Video
Link Demo Aplikasi : https://www.youtube.com/watch?v=0UJRjwcK804 <br>
Link Deploy : http://forumspacing.sanbercodeapp.com/