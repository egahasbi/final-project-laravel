<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Thread;

class Category extends Model
{
    use HasFactory;

    protected $table = "category";
    protected $fillable = ["name", "description", "users_id"];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function thread()
    {
    return $this->hasMany(Thread::class);
    }
}
