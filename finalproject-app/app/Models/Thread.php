<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Thread extends Model
{
    use HasFactory;
    protected $table = "thread";
    protected $fillable = ["title", "category_id", 'content', 'image', 'users_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function reply()
    {
        return $this->hasMany(Reply::class, 'thread_id');
    }
}
