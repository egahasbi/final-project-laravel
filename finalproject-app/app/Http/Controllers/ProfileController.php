<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use File;

class ProfileController extends Controller
{
    public function index()
    {
        $iduser = Auth::id();

        $detailProfile = Profile::where('users_id', $iduser)->first();
        $detailUser = User::where('id', $iduser)->first();

        return view('profile.index', ['detailProfile' => $detailProfile, 'detailUser' => $detailUser]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'age' => 'required',
            'address' => 'required',
            'bio' => 'required',
            'avatar' => 'image|mimes:jpg,png,jpeg',
        ]);

        $profile = Profile::find($id);

        if ($request->has('avatar')) {
            if ($profile->avatar === 'default.png') {
                $path = 'images/';
                $fileName = time() . '.' . $request->avatar->extension();
                $request->avatar->move(public_path('images'), $fileName);
                $profile->avatar = $fileName;
                $profile->save();
            } else {
                $path = 'images/';
                File::delete($path . $profile->avatar);
                $fileName = time() . '.' . $request->avatar->extension();
                $request->avatar->move(public_path('images'), $fileName);
                $profile->avatar = $fileName;
                $profile->save();
            }
        }
        $user = User::find($id);

        $profile->age = $request->age;
        $profile->address = $request->address;
        $profile->bio = $request->bio;
        $user->name = $request->name;
        $user->email = $request->email;

        $profile->save();
        $user->save();

        return redirect('profile');
    }
}
