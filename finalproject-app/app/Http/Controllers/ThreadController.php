<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Models\Thread;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $thread = Thread::all();
        return view('thread.index', compact('thread'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('thread.create', compact('category'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $iduser = Auth::id();

        if($request->has('image')){
            $this->validate($request,[
                'title' => 'required',
                'category_id' => 'required',
                'content' => 'required',
                'image' =>'image|mimes:jpg,png,jpeg'
            ]);
            $fileName = time().'.'.$request->image->extension();

            $request->image->move(public_path('images'), $fileName);

            Thread::create([
                'title' => $request->title,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'users_id' => $iduser,
                'image' => $fileName
            ]);

        } else {
            $this->validate($request,[
                'title' => 'required',
                'category_id' => 'required',
                'content' => 'required'
            ]);

            Thread::create([
                'title' => $request->title,
                'category_id' => $request->category_id,
                'content' => $request->content,
                'users_id' => $iduser
            ]);
        }

    	return redirect('/thread');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $thread = Thread::find($id);
        $category = Category::get();
        return view('thread.detail', ['thread' => $thread, 'category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $thread = Thread::find($id);
        $category = Category::get();
        return view('thread.edit', ['thread' => $thread, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $thread = Thread::find($id);

        if($request->has('image')){
            $this->validate($request,[
                'title' => 'required',
                'category_id' => 'required',
                'content' => 'required',
                'image' => 'required|image|mimes:jpg,png,jpeg'
            ]);

            $path = 'images/';
            File::delete($path . $thread->image);
            $fileName = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $fileName);
            $thread->image = $fileName;
            $thread->update();
        }else{
            $this->validate($request,[
                'title' => 'required',
                'category_id' => 'required',
                'content' => 'required'
            ]);
        }

        $thread = Thread::find($id);
        $thread->title = $request->title;
        $thread->category_id = $request->category_id;
        $thread->content = $request->content;
        $thread->update();
        return redirect('/thread/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $thread = Thread::find($id);
        $thread->delete();
        return redirect('/thread');
        //
    }
}
