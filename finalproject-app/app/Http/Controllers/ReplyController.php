<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reply;
use Illuminate\Support\Facades\Auth;

class ReplyController extends Controller
{
    //
    public function create(Request $request, $id){
        $request->validate([
            'content' => 'required',
        ]);

        $iduser = Auth::id();

        $reply = new Reply;

        $reply->content = $request->content;
        $reply->thread_id = $id;
        $reply->users_id = $iduser;

        $reply->save();

        return redirect('/thread/'. $id);
    }
    public function edit(Request $request, $id){
        $request->validate([
            'content' => 'required',
        ]);

        $iduser = Auth::id();

        $reply = Reply::find($id);

        $reply->content = $request->content;

        $reply->update();

        return redirect('/thread/'. $reply->thread_id);
    }

    public function destroy($id){
        $reply = Reply::find($id);
        $reply->delete();

        return redirect('/thread/'. $reply->thread_id);
    }

}
