<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\ReplyController;
use App\Models\Thread;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'main']);

Route::middleware(['auth'])->group(function () {

    //CRUD Profile
    Route::resource('profile', ProfileController::class)->only(['index','update']);
    Route::resource('category', CategoryController::class)->only(['create','store','edit','destroy','update']);
    Route::resource('thread', ThreadController::class)->only(['create','store','edit','destroy','update']);
    Route::post('/reply/{thread_id}', [ReplyController::class, 'create']);
    Route::put('/reply/{reply_id}', [ReplyController::class, 'edit']);
    Route::delete('/reply/{reply_id}', [ReplyController::class, 'destroy']);

});
Route::resource('category', CategoryController::class)->only(['index']);
Route::resource('thread', ThreadController::class)->only(['index','show']);

Auth::routes();
