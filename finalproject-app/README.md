<img style="text-align:center" src="public/images/carousel2.jpg">

## About Forum Spacing

Forum Spacing is a web forum Laravel framework based. In this forum will discuss various kinds of outer space and various supporting equipment. Please enjoy and explore knowledge as much as possible about outer space and science.

## Authors
* Ega Hasbi Rizqullah
* Danang Iman Panuluh

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
