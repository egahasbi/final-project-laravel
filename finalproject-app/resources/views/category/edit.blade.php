@extends('master')

@section('name')
    Edit Category
@endsection

@section('content')
<div class="container mx-0">
    <form action="/category/{{$category->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{$category->name}}" id="name" placeholder="Input name">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <br>
            <textarea name="description" class="form-control" rows="5">{{$category->description}}</textarea>
            @error('description')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary mb-3">Edit</button>
    </form>
</div>

@endsection
