@extends('master')

@section('title')
    Category List
@endsection

@section('content')
    <a href="/category/create" class="aboutus022 btn">Create</a>
    <div class="question-type2033">
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                    @forelse ($category as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->name}}</td>
                        <td>{{$value->description}}</td>
                        <td>
                            <div class="d-flex">
                            @auth
                                <a href="/category/{{$value->id}}/edit" class="btn btn-primary mx-2">Edit</a>
                                <form action="/category/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn text-white " value="Delete" style="background-color: #ba1e68;">
                                </form>
                            @endauth
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>

@endsection
