@extends('master')

@section('title')
    Create Category
@endsection

@section('content')
<div class="question-type2033">
    <form action="/category" method="POST">
        @csrf
        <div class="form-group">
            <label >Category Name</label>
            <input type="text" class="form-control" name="name" placeholder="Input Category Name">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <br>
            <textarea name="description" class="form-control" rows="5" ></textarea>
            @error('description')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </form>
</div>

@endsection
