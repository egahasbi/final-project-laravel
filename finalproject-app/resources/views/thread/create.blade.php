@extends('master')

@section('title')
    Create Thread
@endsection

@section('content')
<a class="aboutus022 btn" href="/" role="button">Back to Home</a>
<div class="question-type2033">
      <form action="/thread" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label >Title</label>
            <input type="text" class="form-control" name="title">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Category</label>
            <select class="form-control" name="category_id" id="exampleFormControlSelect1">
                @forelse ($category as $value)
                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                @empty
                    <option value="">--Empty Data--</option>
                @endforelse
            </select>
        </div>
        @error('category')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        <div class="form-group">
            <label for="exampleFormControlFile1">Input Image</label>
            <input type="file" class="form-control-file" name="image" id="exampleFormControlFile1">
          </div>
        @error('image')
          <div class="alert alert-danger">
              {{ $message }}
          </div>
        @enderror
        <div class="form-group">
            <label for="content">Content</label>
            <br>
            <textarea name="content" class="form-control" rows="5" ></textarea>
            @error('content')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary mb-3">Submit</button>
    </form>
</div>

@endsection
