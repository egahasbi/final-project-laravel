@extends('master')

@section('title')
    Detail Thread
@endsection

@section('content')
<a class="aboutus022 btn" href="/" role="button">Back to Home</a>
<br>
    <div class="question-type2033">
        <div class="row">
            <div class="col-md-2">
                <div class="container-fluid">
                    <div class="comment-main-level bg-secondary py-3">
                        <div class="comment-box d-flex justify-content-center bg-secondary py-2" >
                            <div class="comment-head bg-secondary text-white">
                                <h5>Thread Starter</h5>
                            </div>
                        </div>
                        <div class="comment-avatar d-flex justify-content-center">
                            <img src={{ asset('images/' . $thread->user->profile->avatar) }} width="150" height="150"
                                class="rounded-circle" alt="">
                        </div>
                        <div class="comment-box d-flex justify-content-center bg-secondary py-2">
                            <div class="comment-head bg-secondary text-white">
                                <ul>
                                    <li>
                                        <h5>{{ $thread->user->name }}</h5>
                                    </li>
                                    <li>Age : {{ $thread->user->profile->age }}</li>
                                    <li>Bio : <br>
                                        {{ $thread->user->profile->bio }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-10">

                <h3>Thread Title : {{ $thread->title }}</h3>
                @if ($thread->image != '')
                    <img src={{ asset('images/' . $thread->image) }} alt="..." style="max-width: 600px;">
                @endif
                <p><br>{{ $thread->content }}</p>
                <hr>
                {{-- <div class="post-footer29032">
                            <div class="l-side2023"><i class="fa fa-folder folder2" aria-hidden="true"> wordpress</i></div>
                        </div> --}}
                Category :<br>
                <h5><span class="badge badge-secondary">{{ $thread->category->name }}</span></h5>
                <hr>
                @auth
                    @if (Auth::user()->id == $thread->users_id)
                        <div class="ques-type302">
                            <form action="/thread/{{ $thread->id }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <a href="/thread/{{ $thread->id }}/edit">
                                    <button type="button" class="btn btn-primary">Edit</button>
                                </a>
                                <input type="submit" class="btn text-white" value="Delete" style="background-color: #ba1e68;">
                            </form>
                        </div>
                    @endif
                @endauth

            </div>
        </div>

    </div>

    <div class="comment-list12993">
        <div class="row">
            <div class="col-md-2">
            </div>
            <div class="col" data-aos="fade-up"
            data-aos-duration="1000">
                <ul id="comments-list" class="comments-list">
                    @forelse ($thread->reply as $item)
                        <div class="comment-main-level">
                            <!-- Avatar -->
                            <div class="comment-avatar"><img src={{ asset('images/' . $item->user->profile->avatar) }}
                                    width="150" height="150" alt=""></div>
                            <!-- Contenedor del Comentario -->
                            <div class="comment-box">
                                <div class="comment-head">
                                    <h6 class="comment-name"><a href="#">{{ $item->user->name }}</a>
                                </div>
                                <div class="comment-content"> {{ $item->content }}
                                </div>
                                
                                <div class="d-flex justify-content-end">
                                    @auth
                                        @if (Auth::user()->id == $item->users_id)
                                            <a class="btn btn-primary btn-sm ml-2" href="#collapseExample{{ $item->id }}"
                                                role="button" data-toggle="collapse" aria-expanded="false"
                                                aria-controls="collapseExample" style="width: 78px">Edit</a>
                                            <form action="/reply/{{ $item->id }}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <input type="submit" class="btn btn-sm ml-2 text-white" style="background-color: #ba1e68; width: 78px"
                                                    value="Delete">
                                            </form>
                                        @endif
                                    @endauth
                                </div>
                                @auth
                                <div class="collapse" id="collapseExample{{ $item->id }}">
                                    <div class="container-fluid">
                                        <form action="/reply/{{ $item->id }}" method="POST">
                                            @csrf
                                            @method('PUT')
                                            <div class="form-group">
                                                <label for="exampleFormControlTextarea1">Edit
                                                    Reply</label>
                                                <textarea class="form-control" id="exampleFormControlTextarea1" name="content" rows="5">{{ $item->content }}</textarea>
                                                @error('content')
                                                    <div class="alert alert-danger">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <button type="submit" class="btn btn-primary mb-3">Submit</button>
                                        </form>
                                    </div>
                                </div>
                                @endauth
                            </div>
                        </div>
                    @empty
                        <p style="text-align: center">There is no reply</p>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
    @auth
        <div class="container-fluid my-5">
            <form action="/reply/{{ $thread->id }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Post Reply</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" name="content" rows="5"></textarea>
                    @error('content')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary mb-3">Submit</button>
            </form>
        </div>
    @endauth
@endsection
