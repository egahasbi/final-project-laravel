@extends('master')

@section('title')
    Thread List
@endsection

@section('content')
    <!--Recent Question Content Section -->
    <a class="aboutus022 btn" href="/thread/create" role="button">Create Thread</a>
    <br>

    <div class="question-type2033">
    @forelse ($thread as $value)
        <li>{{ $value->tittle }}</li>
        <div class="question-type2033">
            <div class="row">
                <div class="col-md-1">
                    <div class="">
                        <img
                            src={{ asset('images/' . $value->user->profile->avatar) }} width="120" height="120" class="rounded-circle"
                            alt="">
                            <h5 style="text-align: center">{{$value->user->name}}</h5>
                    </div>
                </div>
                <div class="col-md-11">
                    <div class="right-description893">
                        <div id="que-hedder2983">
                            <h3><a href="/thread/{{$value->id}}">{{$value->title}}</a></h3>
                        </div>
                        <div class="ques-details10018">
                            <p>{{ $value->content }}</p>
                    </div>
                    <hr>
                    Category :<br>
                    <h5><span class="badge badge-secondary">{{$value->category->name}}</span></h5>
                </div>
            </div>
            {{-- <div class="col-md-2">
                <div class="ques-type302">
                    <a href="/thread/{{$value->id}}/edit">
                        <button type="button" class="btn btn-primary btn-block">Edit</button>
                    </a>
                    <br>
                    <form action="/thread/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger btn-block" value="Delete">
                    </form>
                </div>
            </div> --}}
        </div>
    </div>
    @empty
    <div style="text-align: center" class="my-5">
        <h1>Empty Thread List</h1>
        <br>
    </div>

    @endforelse
    </div>

    {{-- <nav aria-label="Page navigation example">
        <ul class="pagination">
          <li class="page-item"><a class="page-link" href="#">Previous</a></li>
          <li class="page-item"><a class="page-link" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">3</a></li>
          <li class="page-item"><a class="page-link" href="#">Next</a></li>
        </ul>
      </nav>

    <nav aria-label="Page navigation">
        <ul class="pagination">
            <li>
                <a href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
                <a href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav> --}}
@endsection


