@extends('master')

@section('title')
    Profile
@endsection

@section('content')
    <!--Recent Question Content Section -->
    <button type="button" class="aboutus022">Edit Profile</button>
    <br>

    <div class="question-type2033">
        <form action="/profile/{{ $detailProfile->id }}" method="POST" enctype="multipart/form-data">
            <div class="row">

                <div class="col-md-1"></div>


                <div class="col-md-9">

                    @csrf
                    @method('put')

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" value="{{ $detailUser->name }}" class="form-control">
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <label>Email</label>
                        <input type="text" name="email" value="{{ $detailUser->email }}" class="form-control">
                    </div>
                    @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <label>Age</label>
                        <input type="number" name="age" value="{{ $detailProfile->age }}" class="form-control">
                    </div>
                    @error('age')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="address" value="{{ $detailProfile->address }}" class="form-control">
                    </div>
                    @error('address')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <label>Bio</label>
                        <textarea name="bio" id="" class="form-control" cols="30" rows="10">{{ $detailProfile->bio }}</textarea>
                    </div>
                    @error('bio')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <button type="submit" class="btn btn-primary ">Submit</button>

                </div>
                <div class="col-md-2">
                    <div class="container-fluid">
                        <div class="d-flex justify-content-center bg-secondary text-white">
                            <label>
                                <br>
                                {{ $detailUser->name }}
                            </label>
                        </div>
                        <div class="d-flex justify-content-center bg-secondary py-3">

                            <img src={{ asset('images/' . $detailProfile->avatar) }} width="150" height="150"
                                class="rounded-circle">

                        </div>
                        <div class="d-flex justify-content-center bg-secondary py-3 pl-3">

                            <input type="file" name="avatar">
                            @error('avatar')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror

                        </div>
                        <div class="d-flex justify-content-center bg-secondary text-dark">
                            <p>*better upload square photos</p>
                        </div>
                        <div class="d-flex justify-content-center bg-secondary text-white text-center">
                            <label>
                                Age : {{ $detailProfile->age }} years old<br>
                                Address : {{ $detailProfile->address }}
                            </label>

                        </div>
                    </div>
                </div>

            </div>
            @push('scripts')
                <script>
                    Swal.fire({
                        title: "Edit Profile",
                        text: "You can edit your profile here",
                        icon: "info",
                        confirmButtonText: "Ok",
                    });
                </script>
            @endpush
        </form>
    </div>
@endsection
