<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="title" content="Ask online Form">
    <meta name="description"
        content="The Ask is a bootstrap design help desk, support forum website template coded and designed with bootstrap Design, Bootstrap, HTML5 and CSS. Ask ideal for wiki sites, knowledge base sites, support forum sites">
    <meta name="keywords"
        content="HTML, CSS, JavaScript,Bootstrap,js,Forum,webstagram ,webdesign ,website ,web ,webdesigner ,webdevelopment">
    <meta name="robots" content="index, nofollow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <title>Spacing - Let's Explore Together!</title>
    {{-- <link href={{ asset('templates/css/bootstrap.css') }} rel="stylesheet" type="text/css"> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href={{ asset('templates/css/style.css') }} rel="stylesheet" type="text/css">
    <!-- <link href="css/animate.css" rel="stylesheet" type="text/css"> -->
    <link href={{ asset('templates/css/font-awesome.min.css') }} rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> --}}
    {{-- <script src={{asset("templates/js/jquery-3.1.1.min.js")}}></script> --}}
    <script src={{ asset('templates/js/bootstrap.min.js') }}></script>
    <script src={{ asset('templates/js/npm.js') }}></script>
</head>

<body
    style="background-image: url('https://images.unsplash.com/photo-1534796636912-3b95b3ab5986?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80');">
    <nav class="fixed-top navbar-expand-lg sticky-top navbar-dark" style="background-color: #030737;">
        <div class="collapse navbar-collapse" id="navbarNav">
            <a class="navbar-brand" href="/"><img src="{{ asset('images/spacing.png') }}" width="91"
                    height="40" class="d-inline-block mt-1" alt=""></a>
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/profile">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/category">Category</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                @guest
                    <button type="button" class="nav-item rounded" style="background-color: #5643fd;">
                        <a class="nav-link" href="/login">Login</a>
                    </button>
                    <button type="button" class="nav-item rounded" style="background-color: #5643fd;">
                        <a class="nav-link" href="/register">Register</a>
                    </button>
                @endguest

                @auth
                    <button type="button" class="nav-item rounded" style="background-color: #ba1e68;">
                        <a class="nav-link" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </button>
                @endauth
                @auth

                    <a class="nav-link" href="#">{{ Auth::user()->name }}</a>

                @endauth
                @guest
                    <a class="nav-link" href="#"></a>
                @endguest
            </ul>
        </div>
    </nav>
    <section class="welcome-part-one container-fluid"
        style="background-image: url('https://images.unsplash.com/photo-1534796636912-3b95b3ab5986?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxleHBsb3JlLWZlZWR8MXx8fGVufDB8fHx8&w=1000&q=80');">
        <div class="container-fluid d-flex justify-content-center ">
            <div class="bd-example">
                <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel" data-aos="fade-up" data-aos-duration="1750">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                        <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('images/carousel1.png') }}" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block text-light">
                                <h1>Earth, Our Home.</h1>
                                <h4>The Earth is the cradle of humanity, but mankind cannot stay in the cradle forever.
                                </h4>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('images/carousel2.jpg') }}" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block text-light">
                                <h1>Let's Explore!</h1>
                                <h4>That's one small step for a man, one giant leap for mankind.</h4>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('images/carousel3.jpg') }}" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block text-light">
                                <h1>Unlimited Journey for the Future.</h1>
                                <h4>Space is to place as eternity is to time.</h4>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button"
                        data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button"
                        data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>


    </section>
    {{-- <section class="welcome-part-one container-fluid">
            <div class="d-flex flex-row bd-highlight mb-3">
                <div class="p-2 bd-highlight">
                    <a href="#">
                        <button type="button" class="aboutus022">Create Thread</button>
                    </a>
                </div>
                <div class="p-2 bd-highlight">
                    <a href="#">
                        <button type="button" class="join92">Join Now</button>
                    </a>
                </div>

            </div>
        </section> --}}



    <!-- ======content section/body=====-->
    <section class="main-content920" data-aos="fade-up" data-aos-duration="3000">
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-12">
                    <div id="main">
                        <input id="tab1" type="radio" name="tabs" checked>
                        <label for="tab1">@yield('title')</label>
                        <section id="content1">
                            @yield('content')
                        </section>
                        <!--  End of content-1------>
                    </div>
                </div>
                {{-- <div class="col-md-3">
                        <div id="main">
                                <input id="tab2" type="radio" name="tabs2" checked>
                                <label for="tab2"> <a href="/profile" style="text-decoration: none;"> Profile </a></label>
                            <!--  End of content-1------>
                        </div>
                        <div id="main">
                            <input id="tab3" type="radio" name="tabs3" checked>
                            <label for="tab3">Category</label>

                            <!--  End of content-1------>
                        </div> --}}
            </div>
        </div>
        </div>
    </section>
    <!--    footer -->
    <section class="footer-social" style="background-color: #030737;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>Copyright 2022 by Kelompok 10 Batch 40 Laravel Sanbercode | <strong>E.H.R | </strong>
                        <strong>D.I.P</strong>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <script src="{{ asset('/templates/js/swal.min.js') }}"></script>
    @stack('scripts')
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
</body>

</html>
